# What Are You Fit To?

Version 1.0.0

An affirmation to empower and reassure individuals in their skills and abilities. I belong here.

---
## Contributors
* Detravious Brinkley <djbrinkley@outlook.com>

---
## Run Code
1.	Clone Repo
2.  Navigate to the index.html file



---
## Copyright and License

Copyright 2018-2020 FitToCode. Released under the [FTC](https://fittocode.com) license.
